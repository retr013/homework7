class Robot:

    def __init__(self, name, color, wheels_amount, transistors):
        self.color = color
        self.wheels_amount = wheels_amount
        self.transistors = transistors
        self.name = name

    def introduction(self):
        return f"Im {self.name}"


class Terminator(Robot):

    def __init__(self, name, color, wheels_amount, transistors, guns, bullets):
        super().__init__(name, color, wheels_amount, transistors)
        self.guns = guns
        self.bullets = bullets

    def terminate(self):
        self.bullets -= self.guns
        return "Pew pew you're dead"


class BrokenRobot(Robot):

    def __init__(self, name, color, wheels_amount, transistors, audio):
        super().__init__(name, color, wheels_amount, transistors)
        self.audio = audio

    def voice(self):
        return f"Robot says: {self.audio}"



t = Terminator("term", "black", 3, 100, 2, 100)
print(t.terminate())
print(f'{t.bullets} bullet(s) left')