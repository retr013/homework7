import random
import math


class Shape:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def render(self, color):
        print(f'Rendered with color {color}')
        # ...

    def scale(self, scale_factor):
        print(f'Scaled with scale_factor {scale_factor}')
        # ...

    def square(self):
        print('Hello from square()')  # Good default impl?
        return random.randint(1, 10)

    def centre(self):
        return self.x, self.y

    def greater_than(self, other):
        return self.square() < other.square()

    def __gt__(self, other):
        return self.greater_than(other)

    def __contains__(self, other):
        return True


class Circle(Shape):

    def __init__(self, x, y, radius):
        super().__init__(x, y)  # Shape.__init__(self, x, y)
        self.radius = radius

    def square(self):
        return math.pi * 2 * self.radius


class Rectangle(Shape):

    def __init__(self, x, y, height, width):
        super().__init__(x, y)
        self.height = height
        self.width = width

    def print_sides(self):
        print(self.height, self.width)

    def square(self):
        return self.height * self.width


class Parallelogram(Rectangle):

    def __init__(self, x, y, height, width, angle):
        super().__init__(x, y, height, width)
        self.angle = angle

    def print_angle(self):
        print(self.angle)

    def __str__(self):
        return f'Parallelogram: {self.width}, {self.height}, {self.angle}'

    def square(self):
        return self.height * self.width * math.sin(self.angle)



